
%include "lib.inc"

global find_word

section .text

; rdi = key string
; rsi = first element in dictionary
; return:
; rax = pointer to element in dictionary
find_word:
    .loop:             ;
    test rsi, rsi      ; Если указатель нулевой,
    jz .endloop        ; элемент не найден.
                       ;
    push rdi           ;
    push rsi           ;
    add rsi, 8         ; Переводим rsi на строку
    call string_equals ; Сравниваем строки
    pop rsi            ; Восстановление rsi
    pop rdi            ;
                       ;
    test rax, rax      ; Если равны,
    jnz .endloop       ; завершаем поиск.
                       ;
    mov rsi, [rsi]     ; Если не равны, переход к
                       ; следующему элементу.
                       ;
    jmp .loop          ;
    .endloop:          ;
    mov rax, rsi       ;
    ret                ;
