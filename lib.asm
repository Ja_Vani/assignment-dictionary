section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_line

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rcx, rcx
	mov rax, rdi
	.loop:
	mov rax, [rdi+rcx]
	inc rcx
	test al, 0xFF
	jnz .loop
	dec rcx
	mov rax, rcx
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
	call string_length
	mov rdx, rax
	mov rdi, 1
	mov rax, 1
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
	mov rax, 1
	mov rsi, rsp
	mov rdi, 1
	mov rdx, 1
	syscall
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    push rdi
	mov rdi, 0x0A
	jmp print_char
	pop rdi
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
	mov r10, 0x0A
	push 0x00
	.loop:
	xor rdx, rdx
	div r10
	add rdx, '0'
	push rdx
	cmp rax, r10
	jae .loop
	add rax, '0'
	cmp rax, '0'
	je .next
	push rax
	.next:
	pop rdi
	cmp rdi, 0x00
	je .eof
	call print_char
	jmp .next
	.eof:
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov r8, rdi
	test rdi, rdi
	jns .print
	mov rdi, '-'
	call print_char
	mov rdi, r8
	neg rdi
	.print:
	call print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
	.loop:
	mov al, byte[rdi+rcx]
	mov dl, byte[rsi+rcx]
	cmp al, dl
	jne .not_equals
	inc rcx
	cmp al, 0x00
	je .equals
	jmp .loop
	.equals:
	mov rax, 1
	ret
	.not_equals:
	mov rax, 0
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
 	mov rdx, 1
 	mov rdi, 0
 	mov rsi, rsp
 	mov rax, 0
 	syscall
 	pop rax
 	ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rsi                  
    push rdi                  
    call read_char            
    pop rdi                  
    pop rsi                   
    xor rdx, rdx              
    .state_Space:             
        test rax, rax         
        je .react_X2Error     
        cmp rax, 0x20         
        je .react_Space2Space 
        cmp rax, 0x9          
        je .react_Space2Space 
        cmp rax, 0xA          
        je .react_Space2Space 
        cmp rsi, rdx          
        ja .react_Space2Word  
        jmp .react_X2Error    
    .state_Word:              
        cmp rsi, rdx          
        jbe .react_X2Error    
        test rax, rax         
        je .react_Word2End    
        cmp rax, 0x20         
        je .react_Word2End    
        cmp rax, 0x9          
        je .react_Word2End    
        cmp rax, 0xA          
        je .react_Word2End   
        jmp .react_Word2Word  
    .state_End:               
        ret                   
    .state_Error:             
        ret                   
                              
    .react_Space2Space:       
        push rdx              
        push rsi              
        push rdi              
        call read_char        
        pop rdi               
        pop rsi               
        pop rdx               
        jmp .state_Space      
    .react_Space2Word:        
    .react_Word2Word:         
        mov [rdi], al         
        inc rdi               
        inc rdx               
        push rdx              
        push rsi              
        push rdi              
        call read_char        
        pop rdi               
        pop rsi               
        pop rdx               
        jmp .state_Word       
    .react_Word2End:          
        mov [rdi], byte 0     
        mov rax, rdi          
        sub rax, rdx          
        jmp .state_End        
    .react_X2Error:           
        xor rax, rax          
        jmp .state_Error      
    ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
	xor rax, rax
	xor r8, r8
	mov r10, 10
	.num_interval:
	cmp byte[rdi+rcx], '0'
	jb .not_currect
	cmp byte[rdi+rcx], '9'
	ja .not_currect
	mul r10
	mov r8b, byte[rdi+rcx]
	sub r8b, '0'
	add rax, r8
	inc rcx
	jmp .num_interval
	.not_currect:
	mov rdx, rcx
	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
	jne parse_uint
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax            
    .begin:                 
    cmp rdx, 0              
    je .overflow            
    mov cl, byte [rdi]      
    mov [rsi], cl     
	inc rdi      
    inc rax                                     
    inc rsi                 
    dec rdx                 
    cmp cl, 0               
    jne .begin              
    dec rax                 
    ret                     
    .overflow:              
    xor rax, rax            
    ret                 

read_line:
    push rsi                  ;
    push rdi                  ;
    call read_char            ;
    pop rdi                   ;
    pop rsi                   ;
    xor rdx, rdx              ;
    .state_Word:              ;
        cmp rsi, rdx          ; Если нет места для символа --
        jbe .react_X2Error    ; ошибка. (для '\0' тоже нужно место)
        test rax, rax         ; Если символ не считался,
        je .react_Word2End    ; считаем, что ввод завершился.
        cmp rax, 0xA          ; End (место для '\0' есть)
        je .react_Word2End    ;
        jmp .react_Word2Word  ; Если непробельный -- возвращаемся в состояние Word
    .state_End:               ;
        ret                   ;
    .state_Error:             ;
        ret                   ;
                              ;
    .react_Word2Word:         ;
        mov [rdi], al         ; Записать символ в буфер.
        inc rdi               ; Перейти к следующему месту в буфере.
        inc rdx               ; Увеличить количество символов.
        push rdx              ;
        push rsi              ;
        push rdi              ;
        call read_char        ; Переход к следующему символу.
        pop rdi               ;
        pop rsi               ;
        pop rdx               ;
        jmp .state_Word       ;
    .react_Word2End:          ;
        mov [rdi], byte 0     ; Запись символа завершения.
        mov rax, rdi          ; Запись указателя на буфер со
        sub rax, rdx          ; строкой в rax.
        jmp .state_End        ;
    .react_X2Error:           ;
        xor rax, rax          ; В случае ошибки вернуть 0.
        jmp .state_Error      ;
    ret                       ;
