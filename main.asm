%include "dict.inc"
%include "lib.inc"

%define BUFFER_SIZE 256

global _start

section .rodata
    %include "words.inc"
    err_long: db "Error, string too long", 10, 0
    err_none: db "No element in dictionary", 10, 0

section .bss
    line: resb BUFFER_SIZE

section .text
_start:
    mov rdi, line
    mov rsi, BUFFER_SIZE
    call read_line

    test rax, rax
    jnz .read_success
    mov rdi, err_long
    mov rsi, 2
    call print_string
    mov rdi, 1
    jmp exit

    .read_success:
    mov rdi, line
    mov rsi, first_word
    call find_word

    test rax, rax
    jnz .find_success
    mov rdi, err_none
    mov rsi, 2
    call print_string
    mov rdi, 2
    jmp exit

    .find_success:
    add rax, 8             ; skip next address
    .skip_string:          ;
    cmp byte [rax], byte 0 ;
    je .string_skipped     ;
    inc rax                ;
    jmp .skip_string       ;

    .string_skipped:
    inc rax
    mov rdi, rax
    mov rsi, 1
    call print_string
    call print_newline
    mov rdi, 0
    jmp exit

