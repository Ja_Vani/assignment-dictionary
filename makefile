asm := nasm
asmflags := -felf64 -g
exe := exe
srcs := $(wildcard *.asm)
objs := $(patsubst %.asm,%.o,$(srcs))

.PHONY: all
all: $(exe)

.PHONY: clean
clean:
	rm -f *.o
	rm -f $(exe)

%.o: %.asm
	$(asm) $(asmflags) $< -o $@

$(exe) : $(objs)
	ld $^ -o $@
